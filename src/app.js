import {render} from 'react-dom'
import Root from './Application/Root'

render(<Root/>, document.getElementById('app'))