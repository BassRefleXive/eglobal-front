import {route} from '../../config/constants'
import ClientsHandler from './ClientsHandler'
import ClientHandler from './ClientHandler'

export default [
    {
        path: route.ROUTE_CLIENTS,
        component: ClientsHandler,
    },
    {
        path: route.ROUTE_CLIENT,
        component: ClientHandler,
    },
]