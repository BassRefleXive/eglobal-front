import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {
    fetchClientData,
    prepareFetchClientDataRequest,
} from "./../../Flux/Clients/action/fetchClientData";

import {
    createShippingAddress,
    prepareCreateShippingAddress,
} from "./../../Flux/Clients/action/createShippingAddress";

import {
    updateShippingAddress,
} from "./../../Flux/Clients/action/updateShippingAddress";

import {
    deleteShippingAddress,
} from "./../../Flux/Clients/action/deleteShippingAddress";

import {RequestStatus} from './../../config/constants'

import ShippingAddress from './Component/ShippingAddress'

const mapState = ({
    clients: {
        clients,
        shippingAddresses,
        changeRequestStatus,
        fetchRequestStatus: fetchClientDataRequestStatus,
    },
}, {
    router: {
        params: {
            clientId,
        }
    },
}) => {
    const client = clients.get(Number.parseInt(clientId));
    const clientShippingAddresses = client && shippingAddresses.get(client.id);

    return {
        clientId,
        client,
        clientShippingAddresses,
        fetchClientDataRequestStatus,
        changeRequestStatus,
    }
};

const mapDispatch = dispatch => bindActionCreators({
    fetchClientData,
    prepareFetchClientDataRequest,
    createShippingAddress,
    prepareCreateShippingAddress,
    updateShippingAddress,
    deleteShippingAddress,
}, dispatch);

class ClientsHandler extends React.Component {


    componentDidMount() {
        const {
            clientId,
            fetchClientData,
            prepareFetchClientDataRequest,
            fetchClientDataRequestStatus,
        } = this.props;

        switch (fetchClientDataRequestStatus) {
            case RequestStatus.VIRGIN:
                fetchClientData(clientId);
                break;
            case RequestStatus.STARTED:
                break;
            default:
                prepareFetchClientDataRequest()
        }
    }

    componentWillReceiveProps({
        clientId,
        fetchClientData,
        fetchClientDataRequestStatus,
        changeRequestStatus,
    }) {
        if (
            fetchClientDataRequestStatus === RequestStatus.VIRGIN ||
            (changeRequestStatus === RequestStatus.FINISHED && this.props.changeRequestStatus === RequestStatus.STARTED)
        ) {
            fetchClientData(clientId);
        }
    }

    render() {

        console.log(this.props);

        const {
            client,
            clientShippingAddresses,
            createShippingAddress,
            prepareCreateShippingAddress,
            updateShippingAddress,
            deleteShippingAddress,
        } = this.props;


        if (!client) {
            return (
                <div>Cannot obtain client data.</div>
            )
        }

        return (
            <div>
                <div>
                    <div>
                        First name: {client.firstName}
                    </div>
                    <div>
                        Last name: {client.lastName}
                    </div>
                </div>
                <div>
                    <div style={css.address.header.container}>
                        <div style={css.address.header.item}>Default</div>
                        <div style={css.address.header.item}>Country</div>
                        <div style={css.address.header.item}>City</div>
                        <div style={css.address.header.item}>ZIP Code</div>
                        <div style={css.address.header.item}>Street</div>
                        <div style={css.address.header.item}></div>
                    </div>
                    {clientShippingAddresses.map((key, shippingAddress) => {
                        return (
                            <ShippingAddress {...{
                                key,
                                shippingAddress,
                                onSave: data => shippingAddress.id
                                    ? updateShippingAddress(client.id, shippingAddress.id, data)
                                    : createShippingAddress(client.id, data),
                                onDelete: data => deleteShippingAddress(client.id, shippingAddress.id),
                            }} />
                        )
                    }).toArray()}
                </div>
                {clientShippingAddresses.size < 3 && (
                    <div>
                        <a href="#" onClick={ev => ev.preventDefault() || prepareCreateShippingAddress(client.id)}>Create</a>
                    </div>
                )}
            </div>
        );
    }
}

export default connect(mapState, mapDispatch)(ClientsHandler);

const css = {
    component: {
        container: {
            width: '100%',
            textAlign: 'center',
        },
    },
    address: {
        header: {
            container: {
                display: 'flex',
                fontWeight: 'bold',
            },
            item: {
                flexBasis: '20%',
            },
        },
    },
};