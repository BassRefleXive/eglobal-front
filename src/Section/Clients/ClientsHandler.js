import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {
    fetchClientsData,
    prepareFetchClientsDataRequest,
} from "./../../Flux/Clients/action/fetchClientsData";

import {RequestStatus} from './../../config/constants'

const mapState = ({
    clients: {
        clients,
        fetchRequestStatus: fetchClientsDataRequestStatus,
    },
}) => {
    return {
        clients,
        fetchClientsDataRequestStatus
    }
};

const mapDispatch = dispatch => bindActionCreators({
    fetchClientsData,
    prepareFetchClientsDataRequest,
}, dispatch);

class ClientsHandler extends React.Component {


    componentDidMount() {
        const {
            fetchClientsData,
            prepareFetchClientsDataRequest,
            fetchClientsDataRequestStatus,
        } = this.props;

        switch (fetchClientsDataRequestStatus) {
            case RequestStatus.VIRGIN:
                fetchClientsData();
                break;
            case RequestStatus.STARTED:
                break;
            default:
                prepareFetchClientsDataRequest()
        }
    }

    componentWillReceiveProps({
        fetchClientsData,
        fetchClientsDataRequestStatus,
    }) {
        if (fetchClientsDataRequestStatus === RequestStatus.VIRGIN) {
            fetchClientsData();
        }
    }

    render() {

        const {
            clients,
            fetchClientsDataRequestStatus,
        } = this.props;

        if (fetchClientsDataRequestStatus !== RequestStatus.FINISHED) {
            return (
                <div>Loading...</div>
            )
        }

        console.log(clients.toJS())

        return (
            <table style={css.component.container}>
                <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {clients.map(client => {
                    return (
                        <tr {...{key: client.id}}>
                            <td>{client.firstName}</td>
                            <td>{client.lastName}</td>
                            <td>
                                <a href={`/clients/${client.id}`}>Page</a>
                            </td>
                        </tr>
                    )
                }).toArray()}
                </tbody>
            </table>
        );
    }
}

export default connect(mapState, mapDispatch)(ClientsHandler);

const css = {
    component: {
        container: {
            width: '100%',
            textAlign: 'center',
        },
    },
};