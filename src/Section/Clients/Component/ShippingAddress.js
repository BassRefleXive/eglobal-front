import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Input from './../../../Component/Input'

const mapState = ({}) => {
    return {}
};

class ShippingAddress extends React.Component {

    state = {
        editing: false,
        country: undefined,
        city: undefined,
        zipCode: undefined,
        street: undefined,
        default: undefined,
    };

    getDefaultInputValues = () => {
        const {
            shippingAddress,
        } = this.props;

        return {
            country: shippingAddress.country,
            city: shippingAddress.city,
            zipCode: shippingAddress.zipCode,
            street: shippingAddress.street,
            default: shippingAddress.default,
        }
    };

    toggleEdit = () => {
        this.setState({editing: !this.state.editing});
    };

    cancelEdit = () => {
        this.setState(Object.assign(
            {editing: false},
            this.getDefaultInputValues())
        );
    };

    componentDidMount() {
        const {
            shippingAddress,
        } = this.props;

        this.setState(Object.assign(
            {
                editing: !shippingAddress.id,
            },
            this.getDefaultInputValues())
        );
    }

    componentWillReceiveProps({shippingAddress}) {
        this.setState(Object.assign(
            {
                editing: !shippingAddress.id,
            },
            this.getDefaultInputValues())
        );
    }

    onFormSave = () => {
        const {
            country,
            city,
            zipCode,
            street,
            'default': isDefault,
        } = this.state;

        this.props.onSave({
            country,
            city,
            zipCode,
            street,
            'default': isDefault,
        });

        this.toggleEdit();
    };

    render() {

        const {
            onDelete,
        } = this.props;

        const {
            editing,
            country,
            city,
            zipCode,
            street,
            'default': isDefault,
        } = this.state;

        return (
            <div>
                <form style={css.component.form} onSubmit={ev => ev.preventDefault() || this.onFormSave()}>
                    <div style={css.component.item}>
                        {editing ? (
                            <input {...Object.assign(
                                {
                                    type: "checkbox",
                                    onChange: ev => this.setState({default: !isDefault}),
                                    checked: isDefault,
                                }
                            )}
                            />
                        ) : (isDefault ? 'true' : 'false')}
                    </div>
                    <div style={css.component.item}>
                        {editing ? (
                            <Input {...{
                                type: 'text',
                                value: country,
                                onChange: country => this.setState({country}),
                            }}/>
                        ) : country}
                    </div>
                    <div style={css.component.item}>
                        {editing ? (
                            <Input {...{
                                type: 'text',
                                value: city,
                                onChange: city => this.setState({city}),
                            }}/>
                        ) : city}
                    </div>
                    <div style={css.component.item}>
                        {editing ? (
                            <Input {...{
                                type: 'text',
                                value: zipCode,
                                onChange: zipCode => this.setState({zipCode}),
                            }}/>
                        ) : zipCode}
                    </div>
                    <div style={css.component.item}>
                        {editing ? (
                            <Input {...{
                                type: 'text',
                                value: street,
                                onChange: street => this.setState({street}),
                            }}/>
                        ) : street}
                    </div>
                    <div style={css.component.item}>
                        {editing ? (
                            <div>
                                <input type="submit" value="Save"/>
                                <a href="#" onClick={ev => ev.preventDefault() || this.cancelEdit()}>Cancel</a>
                            </div>
                        ) : (
                            <a href="#" onClick={ev => ev.preventDefault() || this.toggleEdit()}>Edit</a>
                        )}
                        <a href="#" onClick={ev => ev.preventDefault() || onDelete()}>Delete</a>
                    </div>
                </form>
            </div>
        )
    }
}

export default connect(mapState)(ShippingAddress);

const css = {
    component: {
        form: {
            display: 'flex',
        },
        item: {
            flexBasis: '20%',
        },
    },
};