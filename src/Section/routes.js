import Application from '../Application/Application'

import clientsRoutes from './Clients/routes'
import landingRoutes from './Landing/routes'

const routes = [
    {
        path: '',
        component: Application,
        childRoutes: [].concat(
            landingRoutes,
            clientsRoutes,
        ),
    },
];

export default routes