const Input = ({
    value,
    type = 'text',
    onChange,
    disabled = false,
}) => (
    <input {...{
        type,
        disabled,
    }} {...onChange ? {
        onChange: ev => onChange(ev.target.value),
        value,
    } : {
        defaultValue: value,
    }}/>
);

export default Input;
