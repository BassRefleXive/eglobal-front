export const route = {
    ROUTE_LANDING: "/",
    ROUTE_CLIENTS: "/clients",
    ROUTE_CLIENT: "/clients/:clientId",
};

export const Http = {
    OK: 200,
    CREATED: 201,
    NO_CONTENT: 204,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    REQUEST_TIMEOUT: 408,
    UNPROCESSABLE_ENTITY: 422,
    INTERNAL_SERVER_ERROR: 500,
};

export const RequestStatus = {
    VIRGIN: 0,
    FINISHED: 1,
    STARTED: 2,
};