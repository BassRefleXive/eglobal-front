import React from 'react';

export default class Component extends React.Component {
    render() {
        return (
            <div style={css.component}>
                {this.props.children}
            </div>
        );
    }
}

const css = {
    component: {
        margin: 20,
    },
};