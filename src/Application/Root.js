import {browserHistory as history} from 'react-router'
import {syncHistoryWithStore} from 'react-router-redux'
import store from "./../Flux/store"
import routes from './../Section/routes'
import {Provider} from 'react-redux'
import {Router} from 'react-router'

const Root = () => (
    <div>
        <Provider {...{store}}>
            <Router {...{
                history: syncHistoryWithStore(history, store),
                routes,
            }}/>
        </Provider>
    </div>
);

export default Root