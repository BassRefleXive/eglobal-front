
import fetch from 'isomorphic-fetch'
import {Http} from './../../config/constants'
import config from './../../config/config'
const TIMEOUT = 30000;

const TYPE_TIMEOUT = "ERROR_TYPE_TIMEOUT";

export const fetchHelper = (...options) => {
    let errorType, url, success, opts, timeout, before;

    if (typeof options[0] === "string") {
        ;([errorType, url, success, opts, timeout] = options)
    } else {
        ;({errorType, url, success, opts, timeout, before} = options[0])
    }

    opts = opts || {};
    timeout = timeout || TIMEOUT;

    if (errorType.indexOf("_ERROR") < 0) {
        console.warn(`Expected *_ERROR type. Got ${errorType}`)
    }

    const fetchOptions = Object.assign({}, {
        credentials: 'include',
    }, opts);

    if (opts.body && !(opts.body instanceof FormData) && (opts.body instanceof Object)) {
        let body = new FormData;

        for (const key in opts.body) {
            body.append(key, opts.body[key])
        }

        fetchOptions.body = body
    }

    return dispatch => {
        if (before) {
            dispatch(before)
        }

        console.info(url, "started with options:", opts)

        const logOnTimeout = setTimeout(() => dispatch({
            status: Http.REQUEST_TIMEOUT,
            error: {
                url,
                timeout,
                opts,
            },
            type: TYPE_TIMEOUT,
        }), timeout);

        return fetch(config.apiBaseUrl + url, fetchOptions)
            .then(response => {
                clearTimeout(logOnTimeout);

                const {
                    status
                } = response;

                const contentType = response.headers.get('Content-Type');

                console.log("http status:", status, "content type:", contentType, "response:", response);

                if (contentType.indexOf("application/json") === -1 && status !== Http.NO_CONTENT) {
                    return dispatch({
                        type: errorType,
                        status,
                        error: {error: `Expected 'application/json'. Got '${contentType}'`},
                    })
                }

                switch (status) {
                    case Http.OK:
                    case Http.CREATED:
                        return response.json()
                            .then(result => {
                                if (!success) {
                                    return dispatch({type: ""})
                                }

                                if (typeof success === "string") {
                                    result = {
                                        type: success
                                    }
                                } else {
                                    result = success(result)
                                }

                                if (!result.type) {
                                    console.warn(`Expected valid type. Got nothing`)
                                } else if (result.type.indexOf("_ERROR") >= 0) {
                                    console.warn(`Not expected *_ERROR type. Got ${result.type}`)
                                }

                                return dispatch(result)
                            })
                            .catch(error => console.error(error));

                    case Http.NO_CONTENT:
                        return response.text()
                            .then(() => {
                                if (!success) {
                                    return dispatch({type: ""})
                                }

                                let result;

                                if (typeof success === "string") {
                                    result = {
                                        type: success
                                    }
                                } else {
                                    result = success(result)
                                }

                                if (!result.type) {
                                    console.warn(`Expected valid type. Got nothing`)
                                } else if (result.type.indexOf("_ERROR") >= 0) {
                                    console.warn(`Not expected *_ERROR type. Got ${result.type}`)
                                }

                                return dispatch(result)
                            })
                            .catch(error => console.error(error));
                    default:
                        return response.json()
                            .then(error => dispatch({
                                type: errorType,
                                status,
                                error,
                            }))
                            .catch(error => console.error(error))
                }
            }, reason => console.log([reason]) || dispatch({
                type: errorType,
                status: reason.status,
                error: {error: reason.message},
            }))
    }
};