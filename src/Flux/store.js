import {createStore, applyMiddleware, compose} from 'redux'
import ReduxThunk from 'redux-thunk'

import reducer from "./reducer"
import init from "Flux/initialState"

const store = createStore(
    reducer,
    init({}, {}),
    compose(applyMiddleware(ReduxThunk))
);

export default store