import {combineReducers} from 'redux'
import {routerReducer as routing} from 'react-router-redux'

import clients from './Clients/store'

export default combineReducers({
    routing,
    clients,
})