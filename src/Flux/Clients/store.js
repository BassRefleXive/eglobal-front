import {Map, Set} from "immutable"

import {initialState} from "./init"
import clientsType from './constants'
import {RequestStatus} from './../../config/constants'

import Client from './Client'
import ShippingAddress from './ShippingAddress'

const hydrateShippingAddressData = ({
    id,
    city,
    country,
    zip_code: zipCode,
    street,
    'default': isDefault,
}) => {
    return new ShippingAddress({
        id,
        city,
        country,
        zipCode,
        street,
        default: isDefault,
    })
};

const hydrateShippingAddressesData = data => {
    let shippingAddresses = Map();

    data.map(clientData => {
        if (clientData.hasOwnProperty('shipping_addresses')) {
            const shippingAddressesData = clientData.shipping_addresses;
            let clientShippingAddresses = Set();

            shippingAddressesData.map(shippingAddressData => {
                clientShippingAddresses = clientShippingAddresses.add(hydrateShippingAddressData(shippingAddressData))
            });

            shippingAddresses = shippingAddresses.set(clientData.id, clientShippingAddresses);
        }
    });

    return shippingAddresses;
};

const hydrateClientData = ({
    id,
    first_name: firstName,
    last_name: lastName,
}) => {
    return new Client({
        id,
        firstName,
        lastName,
    })
};

const hydrateClientsData = data => {
    let clients = Map();

    data.map(clientData => {
        clients = clients.set(clientData.id, hydrateClientData(clientData))
    });

    return clients;
};

const actionsMap = {
    [clientsType.FETCH_CLIENTS_PREPARE]: () => ({fetchRequestStatus: RequestStatus.VIRGIN}),
    [clientsType.FETCH_CLIENTS_START]: () => ({fetchRequestStatus: RequestStatus.STARTED}),
    [clientsType.FETCH_CLIENTS_ERROR]: () => ({fetchRequestStatus: RequestStatus.FINISHED}),
    [clientsType.FETCH_CLIENTS_SUCCESS]: (store, {data}) => {
        return {
            clients: hydrateClientsData(data),
            shippingAddresses: hydrateShippingAddressesData(data),
            fetchRequestStatus: RequestStatus.FINISHED,
        }
    },

    [clientsType.FETCH_CLIENT_PREPARE]: () => ({fetchRequestStatus: RequestStatus.VIRGIN}),
    [clientsType.FETCH_CLIENT_START]: () => ({fetchRequestStatus: RequestStatus.STARTED}),
    [clientsType.FETCH_CLIENT_ERROR]: () => ({fetchRequestStatus: RequestStatus.FINISHED}),
    [clientsType.FETCH_CLIENT_SUCCESS]: (store, {data}) => {
        return {
            clients: hydrateClientsData([data]),
            shippingAddresses: hydrateShippingAddressesData([data]),
            fetchRequestStatus: RequestStatus.FINISHED,
        }
    },

    [clientsType.CREATE_CLIENT_SHIPPING_ADDRESS_START]: () => ({changeRequestStatus: RequestStatus.STARTED}),
    [clientsType.CREATE_CLIENT_SHIPPING_ADDRESS_SUCCESS]: () => ({changeRequestStatus: RequestStatus.FINISHED}),
    [clientsType.CREATE_CLIENT_SHIPPING_ADDRESS_ERROR]: () => ({changeRequestStatus: RequestStatus.FINISHED}),
    [clientsType.CREATE_CLIENT_SHIPPING_ADDRESS_PREPARE]: ({clients, shippingAddresses}, {clientId}) => {
        let clientShippingAddresses = shippingAddresses.get(clientId);

        return {
            shippingAddresses: shippingAddresses.set(clientId, clientShippingAddresses.add(new ShippingAddress())),
        }
    },

    [clientsType.UPDATE_CLIENT_SHIPPING_ADDRESS_START]: () => ({changeRequestStatus: RequestStatus.STARTED}),
    [clientsType.UPDATE_CLIENT_SHIPPING_ADDRESS_SUCCESS]: () => ({changeRequestStatus: RequestStatus.FINISHED}),
    [clientsType.UPDATE_CLIENT_SHIPPING_ADDRESS_ERROR]: () => ({changeRequestStatus: RequestStatus.FINISHED}),

    [clientsType.DELETE_CLIENT_SHIPPING_ADDRESS_START]: () => ({changeRequestStatus: RequestStatus.STARTED}),
    [clientsType.DELETE_CLIENT_SHIPPING_ADDRESS_SUCCESS]: () => ({changeRequestStatus: RequestStatus.FINISHED}),
    [clientsType.DELETE_CLIENT_SHIPPING_ADDRESS_ERROR]: () => ({changeRequestStatus: RequestStatus.FINISHED}),
};

const reducer = (state = initialState, action) => {
    const reduceFn = actionsMap[action.type];

    if (!reduceFn) {
        return state
    }

    return Object.assign({}, state, reduceFn(state, action))
};

export default reducer