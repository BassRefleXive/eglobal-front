import {fetchHelper} from "./../../Action/utils"
import clientsType from './../constants'

export const fetchClientsData = () =>
    fetchHelper({
        errorType: clientsType.FETCH_CLIENTS_ERROR,
        url: `/clients`,
        before: {
            type: clientsType.FETCH_CLIENTS_START,
        },
        success: (data) => ({
            type: clientsType.FETCH_CLIENTS_SUCCESS,
            data,
        })
    });


export const prepareFetchClientsDataRequest = () => ({
    type: clientsType.FETCH_CLIENTS_PREPARE,
});