import {fetchHelper} from "./../../Action/utils"
import clientsType from './../constants'

export const deleteShippingAddress = (clientId, shippingAddressId) =>
    fetchHelper({
        errorType: clientsType.DELETE_CLIENT_SHIPPING_ADDRESS_ERROR,
        url: `/clients/${clientId}/shipping-addresses/${shippingAddressId}`,
        before: {
            type: clientsType.DELETE_CLIENT_SHIPPING_ADDRESS_START,
        },
        opts: {
            method: 'DELETE',
        },
        success: (data) => ({
            type: clientsType.DELETE_CLIENT_SHIPPING_ADDRESS_SUCCESS,
            data,
        })
    });