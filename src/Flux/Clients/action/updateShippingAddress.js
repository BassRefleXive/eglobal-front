import {fetchHelper} from "./../../Action/utils"
import clientsType from './../constants'

export const updateShippingAddress = (clientId, shippingAddressId, data) =>
    fetchHelper({
        errorType: clientsType.UPDATE_CLIENT_SHIPPING_ADDRESS_ERROR,
        url: `/clients/${clientId}/shipping-addresses/${shippingAddressId}`,
        before: {
            type: clientsType.UPDATE_CLIENT_SHIPPING_ADDRESS_START,
        },
        opts: {
            method: 'PUT',
            body: JSON.stringify(data),
        },
        success: (data) => ({
            type: clientsType.UPDATE_CLIENT_SHIPPING_ADDRESS_SUCCESS,
            data,
        })
    });