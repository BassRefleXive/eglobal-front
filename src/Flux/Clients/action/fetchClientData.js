import {fetchHelper} from "./../../Action/utils"
import clientsType from './../constants'

export const fetchClientData = clientId =>
    fetchHelper({
        errorType: clientsType.FETCH_CLIENT_ERROR,
        url: `/clients/${clientId}`,
        before: {
            type: clientsType.FETCH_CLIENT_START,
        },
        success: (data) => ({
            type: clientsType.FETCH_CLIENT_SUCCESS,
            data,
        })
    });


export const prepareFetchClientDataRequest = () => ({
    type: clientsType.FETCH_CLIENT_PREPARE,
});