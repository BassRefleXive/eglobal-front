import {init as initClients} from './Clients/init'

export default function (initialData, data) {
    return {
        clients: initClients(initialData, data)
    }
}